package kz.attractor.foodorderingapp.domain.user;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Table(name = "users")
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class User {
    @NotNull
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Email
    @NotBlank
    @Size(min = 5, max = 128)
    @Column(length = 128)
    private String email;

    @NotBlank
    @Size(min = 8, max = 128)
    @Column(length = 128)
    private String password;

    @NotBlank
    @Size(min = 2, max = 128)
    @Column(length = 128)
    private String fullname;

    @Column
    @Builder.Default
    private boolean active = true;

    @NotBlank
    @Size(min = 2, max = 128)
    @Column(length = 128)
    @Builder.Default
    private String role = "USER";

    public User(String email, String password, String fullname, boolean isActive, String role) {
        this.fullname = fullname;
        this.password = password;
        this.email = email;
        this.active = isActive;
        this.role = role;
    }

    public static User from(UserDTO user) {
        return builder()
                .id(user.getId())
                .email(user.getEmail())
                .fullname(user.getFullname())
                .active(user.isActive())
                .role(user.getRole())
                .build();
    }
}
