package kz.attractor.foodorderingapp.domain.user;

import kz.attractor.foodorderingapp.exceptions.UserAlreadyExistsException;
import kz.attractor.foodorderingapp.exceptions.UserNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder _encoder;

    public List<UserDTO> getUsers() {
        List<User> users = userRepository.findAll();
        return fromListToDtoList(users);
    }

    public UserDTO getUser(Integer id) {
        User user = userRepository.findById(id).orElseThrow(UserNotFoundException::new);
        return UserDTO.from(user);
    }

    public void addUser(String email, String password, String fullname, String status, String role) {
        boolean isActive = status.equals("active");
        password = _encoder.encode(password);
        User user = new User(email, password, fullname, isActive, role);
        userRepository.save(user);
    }

    public void deleteUser(Integer id) {
        userRepository.deleteById(id);
    }

    public void editUser(Integer id, String email, String password, String fullname, String status, String role) {
        User user = userRepository.findById(id).orElseThrow(UserNotFoundException::new);
        if (email != null)
            user.setEmail(email);
        if (password != null)
            user.setPassword(_encoder.encode(password));
        if (fullname != null)
            user.setFullname(fullname);
        if (status != null) {
            boolean isActive = status.equals("active");
            user.setActive(isActive);
        }
        if (role != null)
            user.setRole(role);
        userRepository.save(user);
    }

    public UserDTO register(UserRegisterForm form) {
        if (userRepository.existsByEmail(form.getEmail()))
            throw new UserAlreadyExistsException();
        var user = User.builder()
                .email(form.getEmail())
                .fullname(form.getName())
                .password(_encoder.encode(form.getPassword()))
                .build();
        userRepository.save(user);
        return UserDTO.from(user);
    }

    public UserDTO getByEmail(String email) {
        var user = userRepository.findByEmail(email)
                .orElseThrow(UserNotFoundException::new);
        return UserDTO.from(user);
    }

    public boolean isUserAuthenticated(String email) {
        return userRepository.existsByEmail(email);
    }

    public List<UserDTO> fromListToDtoList(List<User> users) {
        List<UserDTO> userDTOs = new ArrayList<>();
        for (User user : users) {
            userDTOs.add(UserDTO.from(user));
        }
        return userDTOs;
    }
}
