package kz.attractor.foodorderingapp.domain.frontend;

import kz.attractor.foodorderingapp.domain.user.UserDTO;
import kz.attractor.foodorderingapp.domain.user.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/users")
@AllArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping
    public String getUsers(Model model) {
        List<UserDTO> users = userService.getUsers();
        model.addAttribute("users", users);
        return "users";
    }

    @GetMapping("/{id}")
    public String getUser(@PathVariable Integer id,
                          Model model) {
        UserDTO user = userService.getUser(id);
        model.addAttribute("user", user);
        return "user";
    }

    @PostMapping
    public String addUser(@RequestParam String email,
                          @RequestParam String password,
                          @RequestParam String fullname,
                          @RequestParam String status,
                          @RequestParam String role) {
        userService.addUser(email, password, fullname, status, role);
        return "redirect:/users";
    }

    @PostMapping("/{id}/edit")
    public String editUser(@PathVariable Integer id,
                           @RequestParam String email,
                           @RequestParam String password,
                           @RequestParam String fullname,
                           @RequestParam String status,
                           @RequestParam String role) {
        userService.editUser(id, email, password, fullname, status, role);
        return "redirect:/users/{id}";
    }

    @PostMapping("/{id}/delete")
    public String deleteUser(@PathVariable Integer id) {
        userService.deleteUser(id);
        return "redirect:/users";
    }
}
