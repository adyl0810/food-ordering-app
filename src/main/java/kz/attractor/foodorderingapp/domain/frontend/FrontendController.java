package kz.attractor.foodorderingapp.domain.frontend;

import kz.attractor.foodorderingapp.domain.establishment.EstablishmentDTO;
import kz.attractor.foodorderingapp.domain.establishment.EstablishmentService;
import kz.attractor.foodorderingapp.domain.user.UserDTO;
import kz.attractor.foodorderingapp.domain.user.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Controller
@RequestMapping
@AllArgsConstructor
public class FrontendController {

    private final UserService userService;
    private final EstablishmentService establishmentService;

    @GetMapping
    public String index(Principal principal,
                        Model model) {
        if (principal != null) {
            UserDTO user = userService.getByEmail(principal.getName());
            model.addAttribute("user", user);
        }
        return "index";
    }

    @GetMapping("/{id}")
    public String getEstablishment(@PathVariable Integer id,
                                   Model model,
                                   Principal principal) {
        if (principal != null) {
            UserDTO user = userService.getByEmail(principal.getName());
            model.addAttribute("user", user);
        }
        EstablishmentDTO establishment = establishmentService.getEstablishmentById(id);
        model.addAttribute("establishment", establishment);
        return "establishment";
    }
}
