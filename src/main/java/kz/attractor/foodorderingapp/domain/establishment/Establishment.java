package kz.attractor.foodorderingapp.domain.establishment;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "establishments")
@Entity
public class Establishment {
    @NotNull
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank
    @Size(min = 2, max = 128)
    @Column(length = 128)
    private String name;

    @NotBlank
    @Size(min = 2, max = 128)
    @Column(length = 128)
    private String address;

    @NotBlank
    @Size(min = 1, max = 128)
    @Column(length = 128)
    private String image;

    public static Establishment from(EstablishmentDTO establishment) {
        return builder()
                .id(establishment.getId())
                .name(establishment.getName())
                .address(establishment.getAddress())
                .image(establishment.getImage())
                .build();
    }
}
