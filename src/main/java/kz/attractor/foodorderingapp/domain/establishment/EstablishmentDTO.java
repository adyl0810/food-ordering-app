package kz.attractor.foodorderingapp.domain.establishment;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@ToString
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class EstablishmentDTO {

    @NotNull
    private int id;

    @NotBlank
    @Size(min = 2, max = 128)
    private String name;

    @NotBlank
    @Size(min = 2, max = 128)
    private String address;

    @NotBlank
    @Size(min = 1, max = 128)
    private String image;

    public EstablishmentDTO(String name, String address, String image) {
        this.name = name;
        this.address = address;
        this.image = image;
    }

    public static EstablishmentDTO from(Establishment establishment) {
        return builder()
                .id(establishment.getId())
                .name(establishment.getName())
                .address(establishment.getAddress())
                .image(establishment.getImage())
                .build();
    }
}
