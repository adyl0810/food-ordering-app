package kz.attractor.foodorderingapp.domain.establishment;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EstablishmentRepository extends JpaRepository<Establishment, Integer> {
}
