package kz.attractor.foodorderingapp.domain.establishment;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/establishments")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class EstablishmentRestController {

    private final EstablishmentService establishmentService;

    @GetMapping
    public List<EstablishmentDTO> getEstablishments() {
        return establishmentService.getEstablishments();
    }

    @GetMapping("/{id}")
    public EstablishmentDTO getEstablishment(@PathVariable Integer id) {
        return establishmentService.getEstablishmentById(id);
    }

    @PostMapping
    public EstablishmentDTO addEstablishment(@RequestParam String name,
                                             @RequestParam String address,
                                             @RequestParam String image) {
        return establishmentService.addEstablishment(name, address, image);
    }

    @DeleteMapping("/{id}")
    public void deleteEstablishment(@PathVariable Integer id) {
        establishmentService.deleteEstablishment(id);
    }

    @PutMapping("/{id}")
    public EstablishmentDTO editEstablishment(@PathVariable Integer id,
                                  @RequestParam(required = false) String name,
                                  @RequestParam(required = false) String address,
                                  @RequestParam(required = false) String image) {
        return establishmentService.editEstablishment(id, name, address, image);
    }
}
