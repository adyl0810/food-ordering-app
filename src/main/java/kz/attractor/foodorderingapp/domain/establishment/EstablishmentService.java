package kz.attractor.foodorderingapp.domain.establishment;

import kz.attractor.foodorderingapp.exceptions.ResourceNotFoundException;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class EstablishmentService {
    private final EstablishmentRepository establishmentRepository;

    public List<EstablishmentDTO> getEstablishments() {
        List<Establishment> establishments = establishmentRepository.findAll();
        return fromListToDtoList(establishments);
    }

    public EstablishmentDTO getEstablishmentById(Integer id) {
        Establishment establishment = establishmentRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("place", id));
        return EstablishmentDTO.from(establishment);
    }

    public EstablishmentDTO addEstablishment(String name, String address, String image) {
        EstablishmentDTO establishmentDTO = new EstablishmentDTO(name, address, image);
        Establishment establishment = Establishment.from(establishmentDTO);
        establishmentRepository.save(establishment);
        return establishmentDTO;
    }

    public EstablishmentDTO editEstablishment(Integer id, String name, String address, String image) {
        Establishment establishment = establishmentRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("place", id));
        if (name != null)
            establishment.setName(name);
        if (address != null)
            establishment.setAddress(address);
        if (image != null)
            establishment.setImage(image);
        establishmentRepository.save(establishment);
        return EstablishmentDTO.from(establishment);
    }

    public void deleteEstablishment(Integer id) {
        establishmentRepository.deleteById(id);
    }

    public List<EstablishmentDTO> fromListToDtoList(List<Establishment> establishments) {
        List<EstablishmentDTO> establishmentDTOs = new ArrayList<>();
        for (Establishment establishment : establishments) {
            establishmentDTOs.add(EstablishmentDTO.from(establishment));
        }
        return establishmentDTOs;
    }
}
