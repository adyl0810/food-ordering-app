package kz.attractor.foodorderingapp.domain.order;

import kz.attractor.foodorderingapp.domain.food.Food;
import kz.attractor.foodorderingapp.domain.food.FoodDTO;
import kz.attractor.foodorderingapp.domain.food.FoodService;
import kz.attractor.foodorderingapp.domain.user.User;
import kz.attractor.foodorderingapp.domain.user.UserDTO;
import kz.attractor.foodorderingapp.domain.user.UserService;
import kz.attractor.foodorderingapp.exceptions.ResourceNotFoundException;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class OrderService {

    private final OrderRepository orderRepository;
    private final FoodService foodService;
    private final UserService userService;

    public List<OrderDTO> getAllOrders() {
        List<Order> orders = orderRepository.findAll();
        return fromListToDtoList(orders);
    }

    public OrderDTO addOrder(Integer foodId, int quantity, Integer userId) {
        FoodDTO food = foodService.getFood(foodId);
        UserDTO user = userService.getUser(userId);
        OrderDTO orderDTO = new OrderDTO(food, quantity, user, "PENDING");
        orderRepository.save(Order.from(orderDTO));
        return orderDTO;
    }

    public void deleteOrder(Integer id) {
        orderRepository.deleteById(id);
    }

    public OrderDTO editOrder(Integer id, Integer foodId, Integer quantity, Integer userId, String status) {
        Order order = orderRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("order", id));
        if (foodId != null) {
            FoodDTO food = foodService.getFood(foodId);
            order.setFood(Food.from(food));
        }
        if (quantity != null)
            order.setQuantity(quantity);
        if (userId != null) {
            UserDTO user = userService.getUser(userId);
            order.setUser(User.from(user));
        }
        if (status != null)
            order.setStatus(status);
        orderRepository.save(order);
        return OrderDTO.from(order);
    }

    public OrderDTO editOrderQuantity(Integer id, Integer quantity) {
        Order order = orderRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("order", id));
        if (quantity != null)
            order.setQuantity(quantity);
        orderRepository.save(order);
        return OrderDTO.from(order);
    }

    public List<OrderDTO> fromListToDtoList(List<Order> orders) {
        List<OrderDTO> orderDTOs = new ArrayList<>();
        for (Order order : orders) {
            orderDTOs.add(OrderDTO.from(order));
        }
        return orderDTOs;
    }
}
