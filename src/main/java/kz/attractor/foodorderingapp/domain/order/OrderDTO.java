package kz.attractor.foodorderingapp.domain.order;

import kz.attractor.foodorderingapp.domain.food.FoodDTO;
import kz.attractor.foodorderingapp.domain.user.UserDTO;
import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Getter
@Setter
@ToString
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class OrderDTO {

    @NotNull
    private int id;

    @NotNull
    private FoodDTO food;

    @Positive
    @NotBlank
    private int quantity;

    @NotNull
    private UserDTO user;

    @NotBlank
    private String status;

    public OrderDTO(FoodDTO food, int quantity, UserDTO user, String status) {
        this.food = food;
        this.quantity = quantity;
        this.user = user;
        this.status = status;
    }


    public static OrderDTO from(Order order) {
        return builder()
                .id(order.getId())
                .food(FoodDTO.from(order.getFood()))
                .quantity(order.getQuantity())
                .user(UserDTO.from(order.getUser()))
                .status(order.getStatus())
                .build();
    }
}
