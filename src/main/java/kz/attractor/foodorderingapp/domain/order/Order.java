package kz.attractor.foodorderingapp.domain.order;

import kz.attractor.foodorderingapp.domain.food.Food;
import kz.attractor.foodorderingapp.domain.user.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "orders")
@Entity
public class Order {

    @NotNull
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private Food food;

    @NotBlank
    @Positive
    @Builder.Default
    @Column
    private Integer quantity = 1;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    @NotBlank
    @Size(min = 2, max = 32)
    @Column(length = 128)
    @Builder.Default
    private String status = "PENDING";

    public static Order from(OrderDTO order) {
        return builder()
                .id(order.getId())
                .food(Food.from(order.getFood()))
                .quantity(order.getQuantity())
                .user(User.from(order.getUser()))
                .status(order.getStatus())
                .build();
    }

}
