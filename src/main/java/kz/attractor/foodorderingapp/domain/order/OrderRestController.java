package kz.attractor.foodorderingapp.domain.order;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/orders")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class OrderRestController {

    private final OrderService orderService;

    @GetMapping
    public List<OrderDTO> getAllOrders() {
        return orderService.getAllOrders();
    }

    @PostMapping
    public OrderDTO addOrder(@RequestParam Integer foodId,
                             @RequestParam int quantity,
                             @RequestParam Integer userId) {
        return orderService.addOrder(foodId, quantity, userId);
    }

    @DeleteMapping("/{id}")
    public void deleteOrder(@PathVariable Integer id) {
        orderService.deleteOrder(id);
    }

    @PutMapping("/{id}")
    public OrderDTO editOrderQuantity(@PathVariable Integer id,
                                      @RequestParam Integer quantity) {
        return orderService.editOrderQuantity(id, quantity);
    }
}
