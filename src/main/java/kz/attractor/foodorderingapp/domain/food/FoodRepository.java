package kz.attractor.foodorderingapp.domain.food;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FoodRepository extends JpaRepository<Food, Integer> {
    List<Food> findAllByEstablishmentId(Integer id);
}
