package kz.attractor.foodorderingapp.domain.food;

import kz.attractor.foodorderingapp.domain.establishment.Establishment;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "food")
@Entity
public class Food {

    @NotNull
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank
    @Size(min = 2, max = 128)
    @Column(length = 128)
    private String name;

    @NotBlank
    @PositiveOrZero
    @Column
    private float price;

    @NotBlank
    @Size(min = 2, max = 128)
    @Column(length = 128)
    private String description;

    @NotBlank
    @Size(min = 1, max = 128)
    @Column(length = 128)
    private String image;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private Establishment establishment;

    public static Food from(FoodDTO foodDTO) {
        return builder()
                .id(foodDTO.getId())
                .name(foodDTO.getName())
                .price(foodDTO.getPrice())
                .description(foodDTO.getDescription())
                .image(foodDTO.getImage())
                .establishment(Establishment.from(foodDTO.getEstablishment()))
                .build();
    }

}
