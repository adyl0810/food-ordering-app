package kz.attractor.foodorderingapp.domain.food;

import kz.attractor.foodorderingapp.domain.establishment.Establishment;
import kz.attractor.foodorderingapp.domain.establishment.EstablishmentDTO;
import kz.attractor.foodorderingapp.domain.establishment.EstablishmentRepository;
import kz.attractor.foodorderingapp.exceptions.ResourceNotFoundException;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class FoodService {

    private final FoodRepository foodRepository;
    private final EstablishmentRepository establishmentRepository;

    public List<FoodDTO> getAllFood() {
        List<Food> foods = foodRepository.findAll();
        return fromListToDtoList(foods);
    }

    public List<FoodDTO> getFoodByEstablishmentId(Integer id) {
        List<Food> foods = foodRepository.findAllByEstablishmentId(id);
        return fromListToDtoList(foods);
    }

    public FoodDTO getFood(Integer id) {
        Food food = foodRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("food", id));
        return FoodDTO.from(food);
    }

    public FoodDTO addFood(String name, float price, String description, String image, Integer establishmentId) {
        Establishment establishment = establishmentRepository.findById(establishmentId).orElseThrow(() -> new ResourceNotFoundException("place", establishmentId));
        EstablishmentDTO establishmentDTO = EstablishmentDTO.from(establishment);
        FoodDTO foodDTO = new FoodDTO(name, price, description, image, establishmentDTO);
        Food food = Food.from(foodDTO);
        foodRepository.save(food);
        return foodDTO;
    }

    public FoodDTO editFood(Integer id, String name, Integer price, String description, String image) {
        Food food = foodRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("food", id));
        if (name != null)
            food.setName(name);
        if (price != null)
            food.setPrice(price);
        if (description != null)
            food.setDescription(description);
        if (image != null)
            food.setImage(image);
        foodRepository.save(food);
        return FoodDTO.from(food);
    }

    public void deleteFood(Integer id) {
        foodRepository.deleteById(id);
    }

    public List<FoodDTO> fromListToDtoList(List<Food> foods) {
        List<FoodDTO> foodDTOs = new ArrayList<>();
        for (Food food : foods) {
            foodDTOs.add(FoodDTO.from(food));
        }
        return foodDTOs;
    }
}
