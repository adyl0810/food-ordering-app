package kz.attractor.foodorderingapp.domain.food;

import kz.attractor.foodorderingapp.domain.establishment.EstablishmentDTO;
import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

@Getter
@Setter
@ToString
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class FoodDTO {

    @NotNull
    private int id;

    @NotBlank
    @Size(min = 2, max = 128)
    private String name;

    @NotBlank
    @PositiveOrZero
    private float price;

    @NotBlank
    @Size(min = 2, max = 128)
    private String description;

    @NotBlank
    @Size(min = 1, max = 128)
    private String image;

    @NotNull
    private EstablishmentDTO establishment;

    public FoodDTO(String name, float price, String description, String image, EstablishmentDTO establishment) {
        this.name = name;
        this.price = price;
        this.description = description;
        this.image = image;
        this.establishment = establishment;
    }

    public static FoodDTO from(Food food) {
        return builder()
                .id(food.getId())
                .name(food.getName())
                .price(food.getPrice())
                .description(food.getDescription())
                .image(food.getImage())
                .establishment(EstablishmentDTO.from((food.getEstablishment())))
                .build();
    }
}
