package kz.attractor.foodorderingapp.domain.food;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/food")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class FoodRestController {

    private final FoodService foodService;

    @GetMapping
    public List<FoodDTO> getAllFood() {
        return foodService.getAllFood();
    }

    @GetMapping("/{id}")
    public List<FoodDTO> getFoodByEstablishmentId(@PathVariable Integer id) {
        return foodService.getFoodByEstablishmentId(id);
    }

    @PostMapping
    public FoodDTO addFood(@RequestParam String name,
                           @RequestParam float price,
                           @RequestParam String description,
                           @RequestParam String image,
                           @RequestParam Integer establishmentId) {
        return foodService.addFood(name, price, description, image, establishmentId);
    }

    @DeleteMapping("/{id}")
    public void deleteFood(@PathVariable Integer id) {
        foodService.deleteFood(id);
    }

    @PutMapping("/{id}")
    public FoodDTO editFood(@PathVariable Integer id,
                            @RequestParam(required = false) String name,
                            @RequestParam(required = false) Integer price,
                            @RequestParam(required = false) String description,
                            @RequestParam(required = false) String image) {
        return foodService.editFood(id, name, price, description, image);
    }
}
