USE `food_app`;

CREATE TABLE `food`
(
    `id`                INT AUTO_INCREMENT NOT NULL,
    `name`              VARCHAR(128)       NOT NULL,
    `price`             FLOAT              NOT NULL,
    `description`       VARCHAR(128)       NOT NULL,
    `image`             VARCHAR(128)       NOT NULL,
    `establishment_id`  INT                NOT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_food_establishment`
        FOREIGN KEY (`establishment_id`)
            REFERENCES `establishments` (`id`)
);