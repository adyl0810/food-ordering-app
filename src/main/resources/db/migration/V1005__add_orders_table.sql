USE `food_app`;

CREATE TABLE `orders`
(
    `id`       INT AUTO_INCREMENT NOT NULL,
    `food_id`  INT                NOT NULL,
    `quantity` INT                NOT NULL DEFAULT 1,
    `user_id`  INT                NOT NULL,
    `status`   VARCHAR(24)        NOT NULL DEFAULT 'PENDING',
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_orders_food`
        FOREIGN KEY (`food_id`)
            references `food` (`id`),
    CONSTRAINT `fk_orders_user`
        FOREIGN KEY (`user_id`)
            references `users` (`id`)
);