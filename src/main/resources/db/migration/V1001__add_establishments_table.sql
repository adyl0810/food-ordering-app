USE `food_app`;

CREATE TABLE `establishments`
(
    `id`       INT AUTO_INCREMENT NOT NULL,
    `name`     VARCHAR(128)       NOT NULL,
    `address`  VARCHAR(128)       NOT NULL,
    `image`    VARCHAR(128)       NOT NULL,
    PRIMARY KEY (`id`)
);