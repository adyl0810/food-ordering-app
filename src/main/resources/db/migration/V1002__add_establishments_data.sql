USE `food_app`;

INSERT INTO `establishments` (name, address, image)
VALUES ('That Bar', '3646 Whitetail Lane', '1.jpg'),
       ('Juju bar and grill', '3541 Commerce Boulevard', '2.jpg'),
       ('The Rocinante', '4207 Leroy Lane', '3.jpg'),
       ('Brand canteen', '1216 Alexander Drive', '4.jpg'),
       ('Fionn MacCool', '1442 Brownton Road', '5.jpg'),
       ('Nuthouse Grill', '618 Cantebury Drive', '6.jpg'),
       ('McCornlands', '1761 Essex Court', '7.jpg'),
       ('Kickapoo Inn', '4167 Ash Avenue', '8.jpg'),
       ('Capers Cafe Le Bar', '3093 Walton Street', '9.jpg'),
       ('White River Fish Market', '4017 Ingram Road', '10.jpg'),
       ('Mai Thai Cuisine', '2350 Raoul Wallenberg Place', '11.jpg');