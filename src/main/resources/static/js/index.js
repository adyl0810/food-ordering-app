$(document).ready(function () {
    let userRole = $('#userRole').val()
    if (userRole === "ADMIN") {
        const container = $('#container')
        container.prepend(`
            <form id="addEstForm">
                <label for="estNameInput">New establishment name
                    <input type="text" id="estNameInput">
                </label>
                <label for="estAddressInput">New establishment address
                    <input type="text" id="estAddressInput">
                </label>
                <label for="estImageInput">New establishment image
                    <input type="text" id="estImageInput">
                </label>
                <button id="addEstBtn" class="btn btn-primary">Add New Establishment</button>
            </form>
        `)
        $('#addEstForm').on('submit', function (e) {
            e.preventDefault()
            $.ajax({
                type : 'POST',
                url : 'http://localhost:8080/api/establishments',
                data : {
                    name : $('#estNameInput').val(),
                    address : $('#estAddressInput').val(),
                    image : $('#estImageInput').val()
                },
                dataType: 'json',
                success: function (response) {
                    console.log(response);
                }
            })
        })
    }
    $.ajax({
        url : 'http://localhost:8080/api/establishments'
    }).then(function (data) {
        const establishmentsContainer = $('#establishments-container')
        for (let i = 0; i < data.length; i++) {
            establishmentsContainer.append(`
                <div class="col">
                    <div class="card">
                        <img src="images/establishments/${data[i].image}" class="card-img-top" alt="${data[i].image}">
                        <div class="card-body">
                            <h5 class="card-title"><a href="/${data[i].id}">${data[i].name}</a></h5>
                            <p class="card-text">${data[i].address}</p>
                        </div>
                     </div>
                </div>
        `)}
    })
})