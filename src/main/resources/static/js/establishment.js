$(document).ready(function () {
    const urlSplit = $(location).attr("href").split('/')
    const estId = urlSplit[urlSplit.length - 1]
    const estIdInt = parseInt(estId)
    let userRole = $('#userRole').val()
    let userId = $('#userId').val()
    $.ajax({
        url: `http://localhost:8080/api/establishments/${estId}`
    }).then(function (data) {
        const establishmentsContainer = $('#establishments-container')
        if (userRole == null) {
            establishmentsContainer.append(`
            <div class="card mb-3" style="width: 100%;">
                <div class="row g-0">
                    <div class="col-md-4">
                        <img src="images/establishments/${data.image}" class="card-img-top" alt="${data.image}">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h2 class="card-title">${data.name}</h2>
                            <h5 class="card-text">${data.address}</h5>
                            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. A alias asperiores atque blanditiis dicta dignissimos doloremque doloribus earum eius enim eos eum facilis fuga illo, illum incidunt inventore ipsum iure molestiae molestias natus nihil odio officia optio pariatur porro possimus quibusdam reiciendis rem rerum ullam vero voluptatem voluptatum! Amet aspernatur aut blanditiis delectus, ea eaque ipsam maxime quidem repellat ullam! Aliquid et fugiat, laudantium molestiae molestias nemo nulla obcaecati odio optio perferendis quae quaerat qui reprehenderit temporibus voluptates! Asperiores beatae consectetur consequuntur cum dolores ea, expedita id laboriosam mollitia officia repellat rerum similique tempora totam, vel, veritatis voluptatum. Nulla, qui.</p>
                        </div>
                    </div>
                </div>
            </div>
        `)
        } else if (userRole === "ADMIN") {
            establishmentsContainer.append(`
            <div class="card mb-3" style="width: 100%;">
                <div class="row g-0">
                    <div class="col-md-4">
                        <img src="images/establishments/${data.image}" class="card-img-top" alt="${data.image}">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h2 class="card-title">${data.name}</h2>
                            <h5 class="card-text">${data.address}</h5>
                            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. A alias asperiores atque blanditiis dicta dignissimos doloremque doloribus earum eius enim eos eum facilis fuga illo, illum incidunt inventore ipsum iure molestiae molestias natus nihil odio officia optio pariatur porro possimus quibusdam reiciendis rem rerum ullam vero voluptatem voluptatum! Amet aspernatur aut blanditiis delectus, ea eaque ipsam maxime quidem repellat ullam! Aliquid et fugiat, laudantium molestiae molestias nemo nulla obcaecati odio optio perferendis quae quaerat qui reprehenderit temporibus voluptates! Asperiores beatae consectetur consequuntur cum dolores ea, expedita id laboriosam mollitia officia repellat rerum similique tempora totam, vel, veritatis voluptatum. Nulla, qui.</p>
                            <form id="deleteEstForm">
                                <button class="btn btn-primary mb-2" id="deleteEstBtn">Delete Establishment</button>
                            </form>
                            <form id="editEstForm">
                                <label for="estNameInput">Name
                                    <input type="text" id="estNameInput">
                                </label>
                                <label for="estAddressInput">Address
                                    <input type="text" id="estAddressInput">
                                </label>
                                <label for="estImageInput">Image
                                    <input type="text" id="estImageInput">
                                </label>
                                <button id="editEstButton" class="btn btn-primary">Edit Establishment Info</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        `)
            $('#deleteEstForm').on('submit', function (e) {
                e.preventDefault()
                $.ajax({
                    type: 'DELETE',
                    url: `http://localhost:8080/api/establishments/${estId}`,
                }).done(function () {
                    location.href = '/'
                })
            })

            $('#editEstForm').on('submit', function () {
                $.ajax({
                    type: 'PUT',
                    url: `http://localhost:8080/api/establishments/${estId}`,
                    data: {
                        id: estIdInt,
                        name: $('#estNameInput').val(),
                        address: $('#estAddressInput').val(),
                        image: $('#estImageInput').val(),
                    },
                    dataType: 'json'
                })
            })
        }
    })

    $.ajax({
        url: `http://localhost:8080/api/food/${estId}`
    }).done(function (data) {
        const menuContainer = $('#menu-container')
        if (userRole == null) {
            for (let i = 0; i < data.length; i++) {
                menuContainer.append(`
                    <div class="col">
                        <div class="card">
                            <img src="images/food/${data[i].image}" class="card-img-top" alt="${data[i].image}">
                            <div class="card-body">
                                <h5 class="card-title"><a href="/${estId}/${data[i].id}">${data[i].name}</a></h5>
                                <p class="card-text">${data[i].description}</p>
                                <p class="card-text">Price - ${data[i].price}</p>
                                <form class="addToCartForm">
                                    <input type="hidden" value="${data[i].id}" class="foodIdInput">
                                    <label for="quantityInput"> Quantity
                                        <input type="number" class="quantityInput">
                                    </label>
                                    <button id="addToCartBtn">Add To Cart</button>
                                </form>
                            </div>
                        </div>
                    </div>
                `)
                $($('.addToCartForm')[i]).on('submit', function (e) {
                    e.preventDefault()
            $('#submit-order-btn').on('click', function (e) {
                e.preventDefault()
                $('#test').html(`<h2>Your order is received!</h2>`)
            })
                    $.ajax({
                        type : 'POST',
                        url : 'http://localhost:8080/api/orders/',
                        data : {
                            foodId : $($('.foodIdInput')[i]).val(),
                            quantity : $($('.quantityInput')[i]).val(),
                            userId : userId
                        }
                    }).done(function (data) {
                        $('#orderItemsContainer').append(`
                            <p id="order-${data.id}">${data.food.name} x ${data.quantity} <form id="delete-order-form-${data.id}"> <button class="btn btn-danger btn-sm">Remove</button></form></p>
                        `)
                        $(`#delete-order-form-${data.id}`).on('submit', function (e) {
                            e.preventDefault()
                            $('#orderItemsContainer').remove($(`#order-${data.id}`))
                            $.ajax({
                                type : 'DELETE',
                                url : `http://localhost:8080/api/orders/${data.id}`
                            })
                        })
                    })
                })

            }
        }
        else if (userRole === "ADMIN") {
            $('#establishments-container').append(`
                <form id="addFoodForm">
                    <label for="foodNameInputAdd">Name
                        <input type="text" id="foodNameInputAdd">
                    </label>
                    <label for="foodPriceInputAdd">Price
                        <input type="number" id="foodPriceInputAdd">
                    </label>
                    <label for="foodDescriptionInputAdd">Description
                        <input type="text" id="foodDescriptionInputAdd">
                    </label>
                    <label for="foodImageInputAdd">Image
                        <input type="text" id="foodImageInputAdd">
                    </label>
                    <input type="hidden" id="foodEstInput" value="${estIdInt}">
                    <button id="addFoodButton" class="btn btn-primary mb-2">Add Food</button>
                </form>
            `)

            $('#addFoodForm').on('submit', function (e) {
                e.preventDefault()
                $.ajax({
                    type : 'POST',
                    url : 'http://localhost:8080/api/food',
                    data : {
                        name : $('#foodNameInputAdd').val(),
                        price : $('#foodPriceInputAdd').val(),
                        description : $('#foodDescriptionInputAdd').val(),
                        image : $('#foodImageInputAdd').val(),
                        establishmentId : $('#foodEstInput').val()
                    },
                    dataType: 'json'
                })
            })

            for (let i = 0; i < data.length; i++) {
                menuContainer.append(`
                    <div class="col">
                        <div class="card">
                            <img src="images/food/${data[i].image}" class="card-img-top" alt="${data[i].image}">
                            <div class="card-body">
                                <h5 class="card-title"><a href="/${estId}/${data[i].id}">${data[i].name}</a></h5>
                                <p class="card-text">${data[i].description}</p>
                                <p class="card-text">Price - ${data[i].price}</p>
                                <form class="deleteFoodForm">
                                    <button class="btn btn-primary mb-2 deleteFoodBtn">Delete Food</button>
                                </form>
                                <form class="editFoodForm">
                                    <label for="foodNameInput">Name
                                        <input type="text" class="foodNameInput">
                                    </label>
                                    <label for="foodPriceInput">Price
                                        <input type="number" class="foodPriceInput">
                                    </label>
                                    <label for="foodDescriptionInput">Description
                                        <input type="text" class="foodDescriptionInput">
                                    </label>
                                    <label for="foodImageInput">Image
                                        <input type="text" class="foodImageInput">
                                    </label>
                                    <button class="btn btn-primary editFoodButton">Edit Food Info</button>
                                </form>
                            </div>
                        </div>
                    </div>
                `)
                let foodId = data[i].id
                $($('.deleteFoodForm')[i]).on('submit', function () {
                    $.ajax({
                        type: 'DELETE',
                        url: `http://localhost:8080/api/food/${foodId}`,
                    })
                })

                $($('.editFoodForm')[i]).on('submit', function () {
                    $.ajax({
                        type: 'PUT',
                        url: `http://localhost:8080/api/food/${foodId}`,
                        data: {
                            id: foodId,
                            name: $($('.foodNameInput')[i]).val(),
                            price: $($('.foodPriceInput')[i]).val(),
                            description: $($('.foodDescriptionInput')[i]).val(),
                            image : $($('.foodImageInput')[i]).val()
                        },
                        dataType: 'json'
                    })
                })
            }
        }
    })
    $('#test').BootSideMenu();
    $('#demo').BootSideMenu({

        // 'left' or 'right'
        side: "left",

        // animation speed
        duration: 500,

        // restore last menu status on page refresh
        remember: true,

        // auto close
        autoClose: false,

        // push the whole page
        pushBody: true,

        // close on click
        closeOnClick: true,

        // width
        width: "15%",

        // icons
        icons: {
            left: 'glyphicon glyphicon-chevron-left',
            right: 'glyphicon glyphicon-chevron-right',
            down: 'glyphicon glyphicon-chevron-down'
        },

        // 'dracula', 'darkblue', 'zenburn', 'pinklady', 'somebook'
        theme: '',

    });
    $('#demo').BootSideMenu({

        onTogglerClick: function () {
            //code to be executed when the toggler arrow was clicked
        },
        onBeforeOpen: function () {
            //code to be executed before menu open
        },
        onBeforeClose: function () {
            //code to be executed before menu close
        },
        onOpen: function () {
            //code to be executed after menu open
        },
        onClose: function () {
            //code to be executed after menu close
        },
        onStartup: function () {
            //code to be executed when the plugin is called
        }

    });
    // open
//     $('#test').BootSideMenu().open();
//
// // close
//     $('#test').BootSideMenu().close();
//
// // toggle
//     $('#test').BootSideMenu().toggle();
})